function [A,x,y]= Assembly_Helmholtz(xl,xr,yl,yr,N,k2,k)
% assemble the matrix with the grid on [xl,xr]*[yl,yr]
% for the Helmholtz equation defined on a rectangle
%   u_{xx} + u_{yy} + k^2*u = f(x,y)
% with the second order absorbing conditions
%   u_n - 1i*k u - 0.5i/k u_{tt}= 0, on the edges
%   u_{n1} + u_{n2} - 1.5i*k u = 0, at the corners 
% where 'n' is the outward normal direction, 't' is the tagential direction
% 

% without treatment of the boundary conditions
[Dx,Dy,x,y]= chebtorect(xl,xr,yl,yr,N);
Dxx= Dx^2; Dyy= Dy^2;
Id= eye(N+1);
lap= kron(Id,Dxx) + kron(Dyy,Id);
mass= k2.*ones(N+1,N+1);
mass= spdiags(mass(:),0,size(lap,1),size(lap,2));
A= lap + mass;

% now treat the boundary conditions
SIZ= [N+1,N+1];
A= setallabc(A,SIZ,k,Dx,Dy,Dxx,Dyy);

end

