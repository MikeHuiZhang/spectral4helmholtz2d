function [Dx,Dy,x,y]= chebtorect(xl,xr,yl,yr,N)
% compute the differentiation matrix and Chebyshev nodes on [-1,1]
% by the program cheb.m from Treffethen's book 'Spectral methods in Matlab'
% and scale to the cube
[Dref,xref] = cheb(N); 
xref= flipdim(xref,1); % from -1 to 1
Dref= rot90(Dref,2);
% scale them to our geometry
Dx= Dref*2/(xr-xl); Dy= Dref*2/(yr-yl);
x= xl + (xref+1)*(xr-xl)/2; 
y= yl + (xref+1)*(yr-yl)/2; 