function [uh,x,y,c,kx,ky,Phi,G,u] = wavsol(xl,xr,yl,yr,k,xs,ys,kx,ky,ao,x,y)
%u= wavsol(xl,xr,yl,yr,k,fs) solves the Helmholtz equation
%  u_{xx} + u_{yy} + k^2 u = \delta(x-xs,y-ys) in [xl,xr]X[yl,yr]
% equipped with the abosorbing boundary condition
% by the wave based method.
% ao: the order of the abosorbing boundary condition, default 1
%    The second order boundary condition,
%       u_n - 1i*k u -0.5i/k u_{ss} = 0,  in the edges,
%       u_{n1} + u_{n2} - 1.5i*k u  = 0,  at the corners,
%    where 'n', 'n1' and 'n2' are outward normal directions, and 's' is
%    tangential direction. We will treat the $\int u_{ss}*v ds$ by the
%    integration by parts. Assume the s direction is counter-clockwise
%    along the rectangle. The integral becomes
%    $$
%    \int_xl^xr u_{xx}(x,yl) v(x,yl) dx + \int_yl^yr u_{yy}(xr,y)v(xr,y) dy
%    + \int_xl^xr u_{xx}(x,yr)v(x,yr)dx + \int_yl^yr u_{yy}(xl,y)v(xl,y) dy
%   = -\int_xl^xr u_x(x,yl)v_x(x,yl) dx + u_x(x,yl)v(x,yl)|_xl^xr
%     -\int_yl^yr u_y(xr,y)v_y(xr,y) dy + u_y(xr,y)v(xr,y)|_yl^yr
%      -\int_xl^xr u_x(x,yr)v_x(x,yr) dx + u_x(x,yr)v(x,yr)|_xl^xr
%      -\int_yl^yr u_y(xl,y)v_y(xl,y) dy + u_y(xl,y)v(xl,y)|_yl^yr
%   = -\int_s u_s v_s ds + (u_x(xr,yl)-u_y(xr,yl))v(xr,yl) + 
%      (-u_y(xl,yl)-u_x(xl,yl))v(xl,yl) + (u_y(xr,yr)+u_x(xr,yr))v(xr,yr)
%      + (-u_x(xl,yr)+u_y(xl,yr))v(xl,yr)
%   = -\int_s u_s v_s ds + 
%      \sum_{x=xl,xr, y=yl,yr} (u_{n1}(x,y)+u_{n2}(x,y))*v(x,y)
%   = -\int_s u_s v_s ds + 
%      \sum_{x=xl,xr, y=yl,yr} 1.5i*k*u(x,y)*v(x,y).
%    $$
%    In the last equality, the boundary condition at corners are
%    used. The above treatment makes the corner conditions naturally 
%    incorporated into the edge integral and could be easier for evaluation. 
% nkx, nky: number of cosine frequency in x,y directions, resp.
%           default frequencies contain all less than k.
% x, y: 
%    in  the discrete points along x and y directions, resp.
%   out  the discrete mesh points in x-y plane
    
%   remove special solution: the volume equations become homogeneous and 
% the boundary equatoins become inhomogeneous
G= @(x,y) 0.25i*besselh(0,1,k.*sqrt(x.^2+y.^2)); % green function in R2
% We need the fast multipole method to efficiently evaluate the
% convolution for the following 'us' and the derivative of 'us'. 
% us= @(x,y) quad2d(@(xs,ys) -fs(xs,ys).*G(x-xs,y-ys),xl,xr,yl,yr);
% For the moment, we consider point source only and the special solution
% is given by G(x-xs,y-ys) -- the Green function which satisfies
%     \lap u + k^2 u = -\delta(x-xs,y-ys).
% besselh(0,1,z) = besselj(0,z) + 1i*bessely(0,z) 
% the derivative of besselj(0,z) and bessely(0,z) are -besselj(1,z) and
% -bessely(1,z), respectively, so the derivative of besselh(0,1,z) is
% -besselh(1,1,z)
Gx= @(x,y) -0.25i*besselh(1,1,k.*sqrt(x.^2+y.^2)).*k.*x./sqrt(x.^2+y.^2);
Gy= @(x,y) Gx(y,x);
% the boundary value
% -- first order term in the form u_n -1i*k u = g, to be integrated after
%    multiplying test function v
gxl= @(y) Gx(xl-xs,y-ys)+1i*k*G(xl-xs,y-ys); 
gxr= @(y) -Gx(xr-xs,y-ys)+1i*k*G(xr-xs,y-ys); 
gyl= @(x) Gy(x-xs,yl-ys)+1i*k*G(x-xs,yl-ys); 
gyr= @(x) -Gy(x-xs,yr-ys)+1i*k*G(x-xs,yr-ys); 
% -- second order term in the form alpha*u_x(y)=g2, to be integrated after
%     multiplying v_x(y), 
if ~exist('ao','var') || isempty(ao) || ao==1
    alpha= 0;
    beta= 0;
else
    alpha= 0.5i/k; % the minus combined with the minus before int_s
    beta= 0.75; % -1.5i*k*alpha, the coefficients for corners
end
g2xl= @(y) -alpha*Gy(xl-xs,y-ys); % special solution moved to the rhs
g2xr= @(y) -alpha*Gy(xr-xs,y-ys); % so minus
g2yl= @(x) -alpha*Gx(x-xs,yl-ys);
g2yr= @(x) -alpha*Gx(x-xs,yr-ys);
% -- second order term in the form beta*u=g3 at the corners to
%    multiply with values of v
g3xlyl= -beta*G(xl-xs,yl-ys);
g3xlyr= -beta*G(xl-xs,yr-ys);
g3xryl= -beta*G(xr-xs,yl-ys);
g3xryr= -beta*G(xr-xs,yr-ys);

%    discretize the problem with special solution removed, using the plane
% wave basis which are 
%     Phix= cos(k_x x) exp(+(-) sqrt(k_x^2-k^2)(y-yr(yl))),  
%     Phiy= cos(k_y y) exp(+(-) sqrt(k_y^2-k^2)(x-xr(xl))),
kx1= pi/(xr-xl); kkx = floor(k/kx1); kxp= (kkx+1)*kx1; kxm= kkx*kx1; 
ky1= pi/(yr-yl); kky = floor(k/ky1); kyp= (kky+1)*ky1; kym= kky*ky1; 
if ~exist('kx','var') || isempty(kx)
    kx= kx1*(0:kkx);
end
if ~exist('ky','var') || isempty(ky)
    ky= ky1*(0:kky);
end
nkx= length(kx); nky= length(ky);
% - - assemble the matrix of the linear algebra
A= zeros(2*length(kx)+2*length(ky));
% ----- add to A with the integrals on x= xl, xr
% int_y [-(+)D_x Phix(xl(xr),y) -1i*k Phix(xl(xr),y)]*conj(Phix(xl(xr),y))  
[m,n]= ndgrid(1:nkx,1:nkx);
Bx= cos(kx(m)*xl).*(sin(kx(n)*xl).*kx(n)-1i*k*cos(kx(n)*xl)) + ...
    cos(kx(m)*xr).*(-sin(kx(n)*xr).*kx(n)-1i*k*cos(kx(n)*xr));
kxym= conj(sqrt(kx(m).^2-k^2)); kxyn= sqrt(kx(n).^2-k^2); 
temp= kxym + kxyn; % exp + +
By= (1 - exp(temp*(yl-yr)))./temp;
for ii=1:nkx
    if temp(ii,ii)==0
        By(ii,ii)= yr-yl;
    end
end
A(1:nkx,1:nkx)= A(1:nkx,1:nkx) + Bx.*By; 
temp= kxym -kxyn; % + -
By= (exp(kxyn*(yl-yr)) - exp(kxym*(yl-yr)))./temp;
for ii=1:nkx
   if temp(ii,ii)==0
       By(ii,ii)= exp(-kxym(ii,ii)*yr+kxyn(ii,ii)*yl)*(yr-yl);
   end
end
A(1:nkx,nkx+1:2*nkx)= A(1:nkx,nkx+1:2*nkx) + Bx.*By;
temp= -kxym - kxyn; % - -
By= (exp(temp*(yr-yl)) - 1)./temp;
for ii=1:nkx
    if temp(ii,ii)==0
        By(ii,ii)= yr-yl;
    end
end
A(nkx+1:2*nkx,nkx+1:2*nkx)= A(nkx+1:2*nkx,nkx+1:2*nkx) + Bx.*By;
temp= -kxym + kxyn; % - +
By= (exp(kxym*(yl-yr)) - exp(kxyn*(yl-yr)))./temp;
for ii=1:nkx
    if temp(ii,ii)==0
        By(ii,ii)= exp(-kxyn(ii,ii)*yr+kxym(ii,ii)*yl)*(yr-yl);
    end
end
A(nkx+1:2*nkx,1:nkx)= A(nkx+1:2*nkx,1:nkx) + Bx.*By;

% int_y [-(+)D_x Phiy(xl(xr),y) -1i*k Phiy(xl(xr),y)]*conj(Phix(xl(xr),y))
% int(exp(a*y)*cos(b*y)) = exp(ay)*(a*cos(by)+b*sin(by))/(a^2+b^2) 
[m,n]= ndgrid(1:nkx,1:nky);
kxy= conj(sqrt(kx(m).^2-k^2)); kyx= sqrt(ky(n).^2-k^2); 
% kyx= [kyx -kyx; kyx -kyx]; kxy= [kxy kxy; -kxy -kxy];
% m= [m m; m m]; n= [n n; n n];
% A(1:2*nkx,2*nkx+1:2*nkx+2*nky)= A(1:2*nkx,2*nkx+1:2*nkx+2*nky) + ...
%    ((-kyx-1i*k).*exp(kyx*xl).*cos(kx(m)*xl) + ...
%     (kyx-1i*k).*exp(kyx*xr).*cos(kx(m)*xr)) .* ...
%     (exp(kxy*yr).*(kxy.*cos(ky(n)*yr)+ky(n).*sin(ky(n)*yr)) - ...
%      exp(kxy*yl).*(kxy.*cos(ky(n)*yl)+ky(n).*sin(ky(n)*yl))) ./ ...
%     (kxy.^2+ ky(n).^2);
Bxp= (-kyx-1i*k).*exp(kyx*(xl-xr)).*cos(kx(m)*xl) + ...
     (kyx-1i*k).*cos(kx(m)*xr);
Bxm= (kyx-1i*k).*cos(kx(m)*xl) + ...
     (-kyx-1i*k).*exp(kyx*(xl-xr)).*cos(kx(m)*xr);
Byp= ((kxy.*cos(ky(n)*yr)+ky(n).*sin(ky(n)*yr))- ...
     exp(kxy*(yl-yr)).*(kxy.*cos(ky(n)*yl)+ky(n).*sin(ky(n)*yl))) ./ ...
     (kxy.^2+ ky(n).^2);
Bym= (exp(kxy*(yl-yr)).*(-kxy.*cos(ky(n)*yr)+ky(n).*sin(ky(n)*yr)) - ...
     (-kxy.*cos(ky(n)*yl)+ky(n).*sin(ky(n)*yl))) ./ ...
     (kxy.^2+ ky(n).^2);
A(1:nkx,2*nkx+1:2*nkx+nky)= A(1:nkx,2*nkx+1:2*nkx+nky) + ...
    Bxp.*Byp; % exp: +kxy,  +kyx
A(1:nkx,2*nkx+nky+1:2*nkx+2*nky)= A(1:nkx,2*nkx+nky+1:2*nkx+2*nky) + ...
    Bxm.*Byp; % exp: +kxy, -kyx
A(nkx+1:2*nkx,2*nkx+1:2*nkx+nky)= A(nkx+1:2*nkx,2*nkx+1:2*nkx+nky) + ...
    Bxp.*Bym; % exp: -kxy, +kyx
A(nkx+1:2*nkx,2*nkx+nky+1:2*nkx+2*nky)= ... % exp: -kxy, -kyx
    A(nkx+1:2*nkx,2*nkx+nky+1:2*nkx+2*nky) + Bxm.*Bym;    

% int_y [-(+)D_x Phix(xl(xr),y) -1i*k Phix(xl(xr),y)]*conj(Phiy(xl(xr),y))
[m,n]= ndgrid(1:nky,1:nkx);
kyx= conj(sqrt(ky(m).^2-k^2)); kxy= sqrt(kx(n).^2-k^2); 
% kyx= [kyx kyx; -kyx -kyx]; kxy= [kxy -kxy; kxy -kxy];
% m= [m m; m m]; n= [n n; n n];
% A(2*nkx+1:2*nkx+2*nky,1:2*nkx)= A(2*nkx+1:2*nkx+2*nky,1:2*nkx) + ...
%     ((kx(n).*sin(kx(n)*xl)-1i*k*cos(kx(n)*xl)).*exp(kyx*xl) + ...
%      (-kx(n).*sin(kx(n)*xr)-1i*k*cos(kx(n)*xr)).*exp(kyx*xr)) .* ...
%      (exp(kxy*yr).*(kxy.*cos(ky(m)*yr)+ky(m).*sin(ky(m)*yr)) - ...
%       exp(kxy*yl).*(kxy.*cos(ky(m)*yl)+ky(m).*sin(ky(m)*yl))) ./ ...
%      (kxy.^2+ ky(m).^2);
Bxp= (kx(n).*sin(kx(n)*xl)-1i*k*cos(kx(n)*xl)).*exp(kyx*(xl-xr)) + ...
     (-kx(n).*sin(kx(n)*xr)-1i*k*cos(kx(n)*xr));
Bxm= (kx(n).*sin(kx(n)*xl)-1i*k*cos(kx(n)*xl)) + ...
     (-kx(n).*sin(kx(n)*xr)-1i*k*cos(kx(n)*xr)).*exp(kyx*(xl-xr));
Byp= ((kxy.*cos(ky(m)*yr)+ky(m).*sin(ky(m)*yr)) - ...
      exp(kxy*(yl-yr)).*(kxy.*cos(ky(m)*yl)+ky(m).*sin(ky(m)*yl))) ./ ...
     (kxy.^2+ ky(m).^2);
Bym= (exp(kxy*(yl-yr)).*(-kxy.*cos(ky(m)*yr)+ky(m).*sin(ky(m)*yr)) - ...
      (-kxy.*cos(ky(m)*yl)+ky(m).*sin(ky(m)*yl))) ./ ...
     (kxy.^2+ ky(m).^2);
A(2*nkx+1:2*nkx+nky,1:nkx)= A(2*nkx+1:2*nkx+nky,1:nkx) + ...
    Bxp.*Byp; % exp: +kyx, +kxy
A(2*nkx+1:2*nkx+nky,nkx+1:2*nkx)= A(2*nkx+1:2*nkx+nky,nkx+1:2*nkx) + ...
    Bxp.*Bym; % exp: +kyx, -kxy
A(2*nkx+nky+1:2*nkx+2*nky,1:nkx)= A(2*nkx+nky+1:2*nkx+2*nky,1:nkx) + ...
    Bxm.*Byp; % exp: -kyx, +kxy
A(2*nkx+nky+1:2*nkx+2*nky,nkx+1:2*nkx)= ...      % exp: -kyx, -kxy
    A(2*nkx+nky+1:2*nkx+2*nky,nkx+1:2*nkx) + Bxm.*Bym;  

% int_y [-(+)D_x Phiy(xl(xr),y) -1i*k Phiy(xl(xr),y)]*conj(Phiy(xl(xr),y))
% int(cos(a*x)*cos(b*x),x) = (a*cos(b*x)*sin(a*x) -
%                             b*cos(a*x)*sin(b*x))/(a^2 - b^2), a neq b
% if a==b neq 0, x/2 + sin(2*a*x)/(4*a)
% if a==b==0, x
[m,n]= ndgrid(1:nky,1:nky);
kyxm= conj(sqrt(ky(m).^2-k^2)); kyxn= sqrt(ky(n).^2-k^2);
% kyxn= [kyxn -kyxn; kyxn -kyxn]; kyxm= [kyxm kyxm; -kyxm -kyxm];
% temp= kyxn + kyxm; 
% Bx= (exp(temp*xl).*(-kyxn-1i*k) + exp(temp*xr).*(kyxn-1i*k));
% By= ((ky(n).*cos(ky(m)*yr).*sin(ky(n)*yr)-ky(m).*cos(ky(n)*yr).* ...
%       sin(ky(m)*yr)) - (ky(n).*cos(ky(m)*yl).*sin(ky(n)*yl)-ky(m) ...
%       .*cos(ky(n)*yl).*sin(ky(m)*yl))) ./ (ky(n).^2-ky(m).^2);
% for ii= 1:nky 
%     if ky(ii)==0
%         By(ii,ii)= yr-yl;
%     else
%         By(ii,ii)= ((yr-yl)*0.5 + (sin(2*ky(ii)*yr)-sin(2*ky(ii)*yl))./ ...
%                     ky(ii)*0.25);
%     end
% end
% A(2*nkx+1:2*nkx+2*nky,2*nkx+1:2*nkx+2*nky)= ...
%     A(2*nkx+1:2*nkx+2*nky,2*nkx+1:2*nkx+2*nky) + Bx.*[By By; By By];
Bxpp= exp((kyxn+kyxm)*(xl-xr)).*(-kyxn-1i*k) + (kyxn-1i*k);
Bxpm= exp(kyxm*(xl-xr)).*(kyxn-1i*k) + exp(kyxn*(xl-xr)).*(-kyxn-1i*k);
By= ((ky(n).*cos(ky(m)*yr).*sin(ky(n)*yr)-ky(m).*cos(ky(n)*yr).* ...
      sin(ky(m)*yr)) - (ky(n).*cos(ky(m)*yl).*sin(ky(n)*yl)-ky(m) ...
      .*cos(ky(n)*yl).*sin(ky(m)*yl))) ./ (ky(n).^2-ky(m).^2);
for ii= 1:nky 
    if ky(ii)==0
        By(ii,ii)= yr-yl;
    else
        By(ii,ii)= ((yr-yl)*0.5 + (sin(2*ky(ii)*yr)-sin(2*ky(ii)*yl))./ ...
                    ky(ii)*0.25);
    end
end
A(2*nkx+1:2*nkx+2*nky,2*nkx+1:2*nkx+2*nky)= ...
    A(2*nkx+1:2*nkx+2*nky,2*nkx+1:2*nkx+2*nky) + ...
    [Bxpp Bxpm; Bxpm Bxpp].*[By By; By By];
% ------ end of integrals on x= xl,xr
%
% ----- add to A with integrals on y= yl,yr
% int_x (-(+)D_y Phiy(x,yl(yr)) - 1i*k Phiy(x,yl(yr)))*conj(Phiy(x,yl(yr)))
[m,n]= ndgrid(1:nky,1:nky);
By= cos(ky(m)*yl).*(sin(ky(n)*yl).*ky(n)-1i*k*cos(ky(n)*yl)) + ...
    cos(ky(m)*yr).*(-sin(ky(n)*yr).*ky(n)-1i*k*cos(ky(n)*yr));
kyxm= conj(sqrt(ky(m).^2-k^2)); kyxn= sqrt(ky(n).^2-k^2); 
temp= kyxm + kyxn; % exp + +
Bx= (1 - exp(temp*(xl-xr)))./temp;
for ii=1:nky
    if temp(ii,ii)==0
        Bx(ii,ii)= xr-xl;
    end
end
A(2*nkx+1:2*nkx+nky,2*nkx+1:2*nkx+nky)= ...
    A(2*nkx+1:2*nkx+nky,2*nkx+1:2*nkx+nky) + By.*Bx; 
temp= kyxm -kyxn; % + -
Bx= (exp(kyxn*(xl-xr)) - exp(kyxm*(xl-xr)))./temp;
for ii=1:nky
    if temp(ii,ii)==0
        Bx(ii,ii)= exp(-kyxm(ii,ii)*xr+kyxn(ii,ii)*xl)*(xr-xl);
    end
end
A(2*nkx+1:2*nkx+nky,2*nkx+nky+1:2*nkx+2*nky)= ...
    A(2*nkx+1:2*nkx+nky,2*nkx+nky+1:2*nkx+2*nky) + By.*Bx;
temp= -kyxm - kyxn; % - -
Bx= (exp(temp*(xr-xl)) - 1)./temp;
for ii=1:nky
    if temp(ii,ii)==0
        Bx(ii,ii)= xr-xl;
    end
end
A(2*nkx+nky+1:2*nkx+2*nky,2*nkx+nky+1:2*nkx+2*nky)= ...
    A(2*nkx+nky+1:2*nkx+2*nky,2*nkx+nky+1:2*nkx+2*nky) + By.*Bx;
temp= -kyxm + kyxn; % - +
Bx= (exp(kyxm*(xl-xr)) - exp(kyxn*(xl-xr)))./temp;
for ii=1:nky
    if temp(ii,ii)==0
        Bx(ii,ii)= exp(-kyxn(ii,ii)*xr+kyxm(ii,ii)*xl)*(xr-xl);
    end
end
A(2*nkx+nky+1:2*nkx+2*nky,2*nkx+1:2*nkx+nky)= ...
    A(2*nkx+nky+1:2*nkx+2*nky,2*nkx+1:2*nkx+nky) + By.*Bx;

% int_x [-(+)D_y Phix(x,yl(yr)) -1i*k Phix(x,yl(yr))]*conj(Phiy(x,yl(yr)))
% int(exp(a*y)*cos(b*y)) = exp(ay)*(a*cos(by)+b*sin(by))/(a^2+b^2) 
[m,n]= ndgrid(1:nky,1:nkx);
kyx= conj(sqrt(ky(m).^2-k^2)); kxy= sqrt(kx(n).^2-k^2);
Byp= ((-kxy-1i*k).*exp(kxy*(yl-yr)).*cos(ky(m)*yl) + ...
     (kxy-1i*k).*cos(ky(m)*yr));
Bym= (kxy-1i*k).*cos(ky(m)*yl) + ...
     (-kxy-1i*k).*exp(kxy*(yl-yr)).*cos(ky(m)*yr);
Bxp= ((kyx.*cos(kx(n)*xr)+kx(n).*sin(kx(n)*xr))- ...
     exp(kyx*(xl-xr)).*(kyx.*cos(kx(n)*xl)+kx(n).*sin(kx(n)*xl))) ./ ...
     (kyx.^2+ kx(n).^2);
Bxm= (exp(kyx*(xl-xr)).*(-kyx.*cos(kx(n)*xr)+kx(n).*sin(kx(n)*xr)) - ...
     (-kyx.*cos(kx(n)*xl)+kx(n).*sin(kx(n)*xl))) ./ ...
     (kyx.^2+ kx(n).^2);
A(2*nkx+1:2*nkx+nky,1:nkx)= A(2*nkx+1:2*nkx+nky,1:nkx) + ...
    Byp.*Bxp; % exp: +kyx,  +kxy
A(2*nkx+1:2*nkx+nky,nkx+1:2*nkx)= A(2*nkx+1:2*nkx+nky,nkx+1:2*nkx) + ...
    Bym.*Bxp; % exp: +kyx, -kxy
A(2*nkx+nky+1:2*nkx+2*nky,1:nkx)= A(2*nkx+nky+1:2*nkx+2*nky,1:nkx) + ...
    Byp.*Bxm; % exp: -kyx, +kxy
A(2*nkx+nky+1:2*nkx+2*nky,nkx+1:2*nkx)= ... % exp: -kyx, -kxy
    A(2*nkx+nky+1:2*nkx+2*nky,nkx+1:2*nkx) + Bym.*Bxm;    

% int_x [-(+)D_y Phiy(x,yl(yr)) -1i*k Phiy(x,yl(yr))]*conj(Phix(x,yl(yr)))
[m,n]= ndgrid(1:nkx,1:nky);
kxy= conj(sqrt(kx(m).^2-k^2)); kyx= sqrt(ky(n).^2-k^2); 
Byp= (ky(n).*sin(ky(n)*yl)-1i*k*cos(ky(n)*yl)).*exp(kxy*(yl-yr)) + ...
     (-ky(n).*sin(ky(n)*yr)-1i*k*cos(ky(n)*yr));
Bym= (ky(n).*sin(ky(n)*yl)-1i*k*cos(ky(n)*yl)) + ...
     (-ky(n).*sin(ky(n)*yr)-1i*k*cos(ky(n)*yr)).*exp(kxy*(yl-yr));
Bxp= ((kyx.*cos(kx(m)*xr)+kx(m).*sin(kx(m)*xr)) - ...
      exp(kyx*(xl-xr)).*(kyx.*cos(kx(m)*xl)+kx(m).*sin(kx(m)*xl))) ./ ...
     (kyx.^2+ kx(m).^2);
Bxm= (exp(kyx*(xl-xr)).*(-kyx.*cos(kx(m)*xr)+kx(m).*sin(kx(m)*xr)) - ...
      (-kyx.*cos(kx(m)*xl)+kx(m).*sin(kx(m)*xl))) ./ ...
     (kyx.^2+ kx(m).^2);
A(1:nkx,2*nkx+1:2*nkx+nky)= A(1:nkx,2*nkx+1:2*nkx+nky) + ...
    Byp.*Bxp; % exp: +kxy, +kyx
A(1:nkx,2*nkx+nky+1:2*nkx+2*nky)= A(1:nkx,2*nkx+nky+1:2*nkx+2*nky) + ...
    Byp.*Bxm; % exp: +kxy, -kyx
A(nkx+1:2*nkx,2*nkx+1:2*nkx+nky)= A(nkx+1:2*nkx,2*nkx+1:2*nkx+nky) + ...
    Bym.*Bxp; % exp: -kxy, +kyx
A(nkx+1:2*nkx,2*nkx+nky+1:2*nkx+2*nky)= ...      % exp: -kxy, -kyx
    A(nkx+1:2*nkx,2*nkx+nky+1:2*nkx+2*nky) + Bym.*Bxm;  

% int_x [-(+)D_y Phix(yl(yr),x) -1i*k Phix(yl(yr),x)]*conj(Phix(yl(yr),x))
% int(cos(a*y)*cos(b*y),y) = (a*cos(b*y)*sin(a*y) -
%                             b*cos(a*y)*sin(b*y))/(a^2 - b^2), a neq b
% if a==b neq 0, y/2 + sin(2*a*y)/(4*a)
% if a==b==0, y
[m,n]= ndgrid(1:nkx,1:nkx);
kxym= conj(sqrt(kx(m).^2-k^2)); kxyn= sqrt(kx(n).^2-k^2);
Bypp= exp((kxyn+kxym)*(yl-yr)).*(-kxyn-1i*k) + (kxyn-1i*k);
Bypm= exp(kxym*(yl-yr)).*(kxyn-1i*k) + exp(kxyn*(yl-yr)).*(-kxyn-1i*k);
Bx= ((kx(n).*cos(kx(m)*xr).*sin(kx(n)*xr)-kx(m).*cos(kx(n)*xr).* ...
      sin(kx(m)*xr)) - (kx(n).*cos(kx(m)*xl).*sin(kx(n)*xl)-kx(m) ...
      .*cos(kx(n)*xl).*sin(kx(m)*xl))) ./ (kx(n).^2-kx(m).^2);
for ii= 1:nkx 
    if kx(ii)==0
        Bx(ii,ii)= xr-xl;
    else
        Bx(ii,ii)= ((xr-xl)*0.5 + (sin(2*kx(ii)*xr)-sin(2*kx(ii)*xl))./ ...
                    kx(ii)*0.25);
    end
end
A(1:2*nkx,1:2*nkx)= A(1:2*nkx,1:2*nkx) + ...
    [Bypp Bypm; Bypm Bypp].*[Bx Bx; Bx Bx];

% ----- end of integrals on y= yl,yr
%
% ----- add to A with second order terms on x=xl,xr 
if alpha~=0
    % \int_y alpha* D_y Phix(xl(xr),y) * D_y conj(Phix(xl(xr),y))
    [m,n]= ndgrid(1:nkx,1:nkx);
    kxym= conj(sqrt(kx(m).^2-k^2)); kxyn= sqrt(kx(n).^2-k^2);
    Bx= cos(kx(m)*xl).*cos(kx(n)*xl) + cos(kx(m)*xr).*cos(kx(n)*xr);
    temp= kxym + kxyn; % + +
    By= alpha*kxym.*kxyn.*(1-exp(temp*(yl-yr)))./temp;
    for ii= 1:nkx
        if temp(ii,ii)==0
            By(ii,ii)= alpha*kxym(ii,ii)*kxyn(ii,ii)*(yr-yl);
        end
    end
    A(1:nkx,1:nkx)= A(1:nkx,1:nkx) + Bx.*By;
    temp= kxym - kxyn; % + -
    By= alpha*kxym.*(-kxyn).*(exp(kxyn*(yl-yr))-exp(kxym*(yl-yr)))./temp;
    for ii= 1:nkx
        if temp(ii,ii)==0
            By(ii,ii)= alpha*kxym(ii,ii)*(-kxyn(ii,ii))*(yr-yl) ...
                *exp(-kxym(ii,ii)*yr+kxyn(ii,ii)*yl);
        end
    end
    A(1:nkx,nkx+1:2*nkx)= A(1:nkx,nkx+1:2*nkx) + Bx.*By;
    temp= -kxym -kxyn; % - -
    By= alpha*kxym.*kxyn.*(exp(temp*(yr-yl))-1)./temp;
    for ii= 1:nkx
        if temp(ii,ii)==0
            By(ii,ii)= alpha*kxym(ii,ii)*kxyn(ii,ii)*(yr-yl);
        end
    end
    A(nkx+1:2*nkx,nkx+1:2*nkx)= A(nkx+1:2*nkx,nkx+1:2*nkx) + Bx.*By;
    temp= -kxym +kxyn; % - +
    By= alpha*(-kxym).*kxyn.*(exp(kxym*(yl-yr))-exp(kxyn*(yl-yr)))./temp;
    for ii= 1:nkx
        if temp(ii,ii)==0
            By(ii,ii)= alpha*(-kxym(ii,ii))*kxyn(ii,ii)*(yr-yl) ...
                *exp(-kxyn(ii,ii)*yr+kxym(ii,ii)*yl);
        end
    end
    A(nkx+1:2*nkx,1:nkx)= A(nkx+1:2*nkx,1:nkx) + Bx.*By;
   
    % \int_y alpha* D_y Phiy * D_y conj(Phix)
    % int(exp(a*x)*(-sin(b*x)),x)= 
    %     (exp(a*x)*(b*cos(b*x) - a*sin(b*x)))/(a^2 + b^2)
    [m,n]= ndgrid(1:nkx,1:nky);
    kxy= conj(sqrt(kx(m).^2-k^2)); kyx= sqrt(ky(n).^2-k^2);
    % kxy= [kxy kxy; -kxy -kxy]; kyx= [kyx -kyx; kyx -kyx];
    % m= [m m; m m]; n= [n n; n n];
    % A(1:2*nkx,2*nkx+1:2*nkx+2*nky)= A(1:2*nkx,2*nkx+1:2*nkx+2*nky) + ...
    %   (exp(kyx*xl).*cos(kx(m)*xl)+exp(kyx*xr).*cos(kx(m)*xr)).*ky(n) ...
    %   .*kxy.*(exp(kxy*yr).*(ky(n).*cos(ky(n)*yr) - kxy.*sin(ky(n)*yr)) ...
    %          -exp(kxy*yl).*(ky(n).*cos(ky(n)*yl) - kxy.*sin(ky(n)*yl))) ...
    %   ./(kxy.^2+ky(n).^2);
    Bxp= exp(kyx*(xl-xr)).*cos(kx(m)*xl) + cos(kx(m)*xr);
    Bxm= cos(kx(m)*xl) + exp(kyx*(xl-xr)).*cos(kx(m)*xr);
    Byp= ky(n).*kxy.* ...
         ((ky(n).*cos(ky(n)*yr) - kxy.*sin(ky(n)*yr)) ...
         -exp(kxy*(yl-yr)).*(ky(n).*cos(ky(n)*yl) - kxy.*sin(ky(n)*yl))) ...
        ./(kxy.^2+ky(n).^2);
    Bym= ky(n).*(-kxy).* ...
         (exp(kxy*(yl-yr)).*(ky(n).*cos(ky(n)*yr) + kxy.*sin(ky(n)*yr)) ...
          -(ky(n).*cos(ky(n)*yl) + kxy.*sin(ky(n)*yl))) ...
        ./(kxy.^2+ky(n).^2);
    B= [Bxp Bxm; Bxp Bxm].*[Byp Byp; Bym Bym];
    A(1:2*nkx,2*nkx+1:2*nkx+2*nky)= A(1:2*nkx,2*nkx+1:2*nkx+2*nky)+alpha*B;
    
    % \int_y alpha* D_y Phix * D_y conj(Phiy)
    % [m,n]= ndgrid(1:nky,1:nkx);
    % kxy= conj(sqrt(kx(n).^2-k^2)); kyx= sqrt(ky(m).^2-k^2);
    % kxy= [kxy -kxy; kxy -kxy]; kyx= [kyx kyx; -kyx -kyx];
    % m= [m m; m m]; n= [n n; n n];
    % A(2*nkx+1:2*nkx+2*nky,1:2*nkx)= A(2*nkx+1:2*nkx+2*nky,1:2*nkx) + ...
    %   (exp(kyx*xl).*cos(kx(n)*xl)+exp(kyx*xr).*cos(kx(n)*xr)).*ky(m) ...
    %   .*kxy.*(exp(kxy*yr).*(ky(m).*cos(ky(m)*yr) - kxy.*sin(ky(m)*yr)) ...
    %          -exp(kxy*yl).*(ky(m).*cos(ky(m)*yl) - kxy.*sin(ky(m)*yl))) ...
    %   ./(kxy.^2+ky(m).^2);
    A(2*nkx+1:2*nkx+2*nky,1:2*nkx)= A(2*nkx+1:2*nkx+2*nky,1:2*nkx)+alpha*B';

    % \int_y alpha* D_y Phiy * D_y conj(Phiy)
    %  int(sin(a*x)*sin(b*x),x)=
    %   -(a*cos(a*x)*sin(b*x) - b*cos(b*x)*sin(a*x))/(a^2 - b^2), a neq b
    %   a==b neq 0, x/2 - sin(2*a*x)/(4*a)
    %   a==b==0,    0
    [m,n]= ndgrid(1:nky,1:nky);
    By= ky(m).*ky(n).*((ky(m).*cos(ky(m)*yl).*sin(ky(n)*yl)- ...
                        ky(n).*cos(ky(n)*yl).*sin(ky(m)*yl))...
                     - (ky(m).*cos(ky(m)*yr).*sin(ky(n)*yr)- ...
                        ky(n).*cos(ky(n)*yr).*sin(ky(m)*yr)))...
        ./(ky(m).^2-ky(n).^2);
    for ii= 1:nky
        if ky(ii)==0
            By(ii,ii)= 0;
        else
            By(ii,ii)= (yr-yl)/2-(sin(2*ky(ii)*yr)-sin(2*ky(ii)*yl))...
                /ky(ii)/4;
        end
    end
    kyxm= conj(sqrt(ky(m).^2-k^2)); kyxn= sqrt(ky(n).^2-k^2);
    % temp= [kyxm+kyxn, kyxm-kyxn; -kyxm+kyxn, -kyxm-kyxn];
    % Bx= exp(temp*xl) + exp(temp*xr);
    % A(2*nkx+1:2*nkx+2*nky,2*nkx+1:2*nkx+2*nky)= ...
    %     A(2*nkx+1:2*nkx+2*nky,2*nkx+1:2*nkx+2*nky)+Bx.*[By By;By By];
    Bxpp= exp((kyxm+kyxn)*(xl-xr)) + 1;
    Bxpm= exp(kyxm*(xl-xr)) + exp(kyxn*(xl-xr));
    A(2*nkx+1:2*nkx+2*nky,2*nkx+1:2*nkx+2*nky)= ...
        A(2*nkx+1:2*nkx+2*nky,2*nkx+1:2*nkx+2*nky) + ...
        alpha*[Bxpp Bxpm; Bxpm Bxpp].*[By By;By By];
    % ---- end of second order term on x=xl, xr
    %
    % ---- add to A with second order term on y=yl,yr
    % \int_x alpha* D_x Phiy(yl(yr),x) * D_x conj(Phiy(yl(yr),x))
    [m,n]= ndgrid(1:nky,1:nky);
    kyxm= conj(sqrt(ky(m).^2-k^2)); kyxn= sqrt(ky(n).^2-k^2);
    By= cos(ky(m)*yl).*cos(ky(n)*yl) + cos(ky(m)*yr).*cos(ky(n)*yr);
    temp= kyxm + kyxn; % + +
    Bx= alpha*kyxm.*kyxn.*(1-exp(temp*(xl-xr)))./temp;
    for ii= 1:nky
        if temp(ii,ii)==0
            Bx(ii,ii)= alpha*kyxm(ii,ii)*kyxn(ii,ii)*(xr-xl);
        end
    end
    A(2*nkx+(1:nky),2*nkx+(1:nky))= A(2*nkx+(1:nky),2*nkx+(1:nky)) + By.*Bx;
    temp= kyxm - kyxn; % + -
    Bx= alpha*kyxm.*(-kyxn).*(exp(kyxn*(xl-xr))-exp(kyxm*(xl-xr)))./temp;
    for ii= 1:nky
        if temp(ii,ii)==0
            Bx(ii,ii)= alpha*kyxm(ii,ii)*(-kyxn(ii,ii))*(xr-xl) ...
                *exp(-kyxm(ii,ii)*xr+kyxn(ii,ii)*xl);
        end
    end
    A(2*nkx+(1:nky),2*nkx+(nky+1:2*nky))= ...
        A(2*nkx+(1:nky),2*nkx+(nky+1:2*nky)) + By.*Bx;
    temp= -kyxm -kyxn; % - -
    Bx= alpha*kyxm.*kyxn.*(exp(temp*(xr-xl))-1)./temp;
    for ii= 1:nky
        if temp(ii,ii)==0
            Bx(ii,ii)= alpha*kyxm(ii,ii)*kyxn(ii,ii)*(xr-xl);
        end
    end
    A(2*nkx+(nky+1:2*nky),2*nkx+(nky+1:2*nky))= ...
        A(2*nkx+(nky+1:2*nky),2*nkx+(nky+1:2*nky)) + By.*Bx;
    temp= -kyxm +kyxn; % - +
    Bx= alpha*(-kyxm).*kyxn.*(exp(kyxm*(xl-xr))-exp(kyxn*(xl-xr)))./temp;
    for ii= 1:nky
        if temp(ii,ii)==0
            Bx(ii,ii)= alpha*(-kyxm(ii,ii))*kyxn(ii,ii)*(xr-xl) ...
                *exp(-kyxn(ii,ii)*xr+kyxm(ii,ii)*xl);
        end
    end
    A(2*nkx+(nky+1:2*nky),2*nkx+(1:nky))= ...
        A(2*nkx+(nky+1:2*nky),2*nkx+(1:nky)) + By.*Bx;
   
    % \int_x alpha* D_x Phix * D_x conj(Phiy)
    % int(exp(a*y)*(-sin(b*y)),y)= 
    %     (exp(a*y)*(b*cos(b*y) - a*sin(b*y)))/(a^2 + b^2)
    [m,n]= ndgrid(1:nky,1:nkx);
    kyx= conj(sqrt(ky(m).^2-k^2)); kxy= sqrt(kx(n).^2-k^2);
    Byp= exp(kxy*(yl-yr)).*cos(ky(m)*yl)+cos(ky(m)*yr);
    Bym= cos(ky(m)*yl)+exp(kxy*(yl-yr)).*cos(ky(m)*yr);
    Bxp= kx(n).*kyx.* ...
         ((kx(n).*cos(kx(n)*xr) - kyx.*sin(kx(n)*xr)) ...
         -exp(kyx*(xl-xr)).*(kx(n).*cos(kx(n)*xl) - kyx.*sin(kx(n)*xl))) ...
        ./(kyx.^2+kx(n).^2);
    Bxm= kx(n).*(-kyx).* ...
         (exp(kyx*(xl-xr)).*(kx(n).*cos(kx(n)*xr) + kyx.*sin(kx(n)*xr)) ...
          -(kx(n).*cos(kx(n)*xl) + kyx.*sin(kx(n)*xl))) ...
        ./(kyx.^2+kx(n).^2);
    B= [Byp Bym; Byp Bym].*[Bxp Bxp; Bxm Bxm];
    A(2*nkx+1:2*nkx+2*nky,1:2*nkx)= A(2*nkx+1:2*nkx+2*nky,1:2*nkx) + ...
        alpha*B;
       
    % \int_x alpha* D_x Phiy * D_x conj(Phix)
    A(1:2*nkx,2*nkx+1:2*nkx+2*nky)= A(1:2*nkx,2*nkx+1:2*nkx+2*nky) + ...
        alpha*B';

    % \int_x alpha* D_x Phix * D_x conj(Phix)
    %  int(sin(a*y)*sin(b*y),y)=
    %   -(a*cos(a*y)*sin(b*y) - b*cos(b*y)*sin(a*y))/(a^2 - b^2), a neq b
    %   a==b neq 0, y/2 - sin(2*a*y)/(4*a)
    %   a==b==0,    0
    [m,n]= ndgrid(1:nkx,1:nkx);
    Bx= kx(m).*kx(n).*((kx(m).*cos(kx(m)*xl).*sin(kx(n)*xl)- ...
                        kx(n).*cos(kx(n)*xl).*sin(kx(m)*xl))...
                     - (kx(m).*cos(kx(m)*xr).*sin(kx(n)*xr)- ...
                        kx(n).*cos(kx(n)*xr).*sin(kx(m)*xr)))...
        ./(kx(m).^2-kx(n).^2);
    for ii= 1:nkx
        if kx(ii)==0
            Bx(ii,ii)= 0;
        else
            Bx(ii,ii)= (xr-xl)/2-(sin(2*kx(ii)*xr)-sin(2*kx(ii)*xl))...
                /kx(ii)/4;
        end
    end
    kxym= conj(sqrt(kx(m).^2-k^2)); kxyn= sqrt(kx(n).^2-k^2);
    Bypp= exp((kxym+kxyn)*(yl-yr)) + 1;
    Bypm= exp(kxym*(yl-yr)) + exp(kxyn*(yl-yr));
    A(1:2*nkx,1:2*nkx)= A(1:2*nkx,1:2*nkx) + ...
        alpha* [Bypp Bypm; Bypm Bypp].*[Bx Bx; Bx Bx];
    % ---- end of second order term on y= yl, yr
    %
    % ---- add to A with second order term at the corners
    % beta * Phix * conj(Phix)
    [m,n]= ndgrid(1:nkx,1:nkx);
    kxym= conj(sqrt(kx(m).^2-k^2)); kxyn= sqrt(kx(n).^2-k^2);
    % temp= [kxym+kxyn, kxym-kxyn; -kxym+kxyn, -kxym-kxyn];
    % Bx= cos(kx(m)*xl).*cos(kx(n)*xl) + cos(kx(m)*xr).*cos(kx(n)*xr); 
    % Bx=  [Bx Bx; Bx Bx]; 
    % A(1:2*nkx,1:2*nkx)= A(1:2*nkx,1:2*nkx)  ...
    %     -1.5i*k*alpha*Bx.*(exp(temp*yl)+exp(temp*yr));
    Bx= cos(kx(m)*xl).*cos(kx(n)*xl) + cos(kx(m)*xr).*cos(kx(n)*xr); 
    Bypp= exp((kxym+kxyn)*(yl-yr)) + 1;
    Bypm= exp(kxym*(yl-yr)) + exp(kxyn*(yl-yr));
    A(1:2*nkx,1:2*nkx)= A(1:2*nkx,1:2*nkx)  ...
        + beta* [Bx Bx; Bx Bx].*[Bypp Bypm; Bypm Bypp];

    % beta* Phiy * conj(Phix)
    [m,n]= ndgrid(1:nkx,1:nky);
    kxy= conj(sqrt(kx(m).^2-k^2)); kyx= sqrt(ky(n).^2-k^2);
    % kxy= [kxy kxy; -kxy, -kxy]; kyx= [kyx, -kyx; kyx, -kyx];
    % m= [m m; m m];n= [n n; n n];
    % A(1:2*nkx,2*nkx+1:2*nkx+2*nky)= A(1:2*nkx,2*nkx+1:2*nkx+2*nky)  ...
    %     -1.5i*k*alpha* ...
    %     (cos(kx(m)*xl).*exp(kyx*xl) + cos(kx(m)*xr).*exp(kyx*xr)).* ...
    %     (exp(kxy*yl).*cos(ky(n)*yl) + exp(kxy*yr).*cos(ky(n)*yr));
    Bxp= cos(kx(m)*xl).*exp(kyx*(xl-xr)) + cos(kx(m)*xr);
    Bxm= cos(kx(m)*xl) + cos(kx(m)*xr).*exp(kyx*(xl-xr));
    Byp= exp(kxy*(yl-yr)).*cos(ky(n)*yl) + cos(ky(n)*yr);
    Bym= cos(ky(n)*yl) + exp(kxy*(yl-yr)).*cos(ky(n)*yr);
    B= [Bxp Bxm; Bxp Bxm].*[Byp Byp; Bym Bym];
    A(1:2*nkx,2*nkx+1:2*nkx+2*nky)= A(1:2*nkx,2*nkx+1:2*nkx+2*nky)  ...
        + beta* B;                  
   
    % beta * Phix * conj(Phiy)
    % [m,n]= ndgrid(1:nky,1:nkx);
    % kyx= conj(sqrt(ky(m).^2-k^2)); kxy= sqrt(kx(n).^2-k^2);
    % kyx= [kyx kyx; -kyx, -kyx]; kxy= [kxy, -kxy; kxy, -kxy];
    % m= [m m; m m];n= [n n; n n];
    % A(2*nkx+1:2*nkx+2*nky,1:2*nkx)= A(2*nkx+1:2*nkx+2*nky,1:2*nkx)  ...
    %     -1.5i*k*alpha* ...
    %     (cos(ky(m)*yl).*exp(kxy*yl) + cos(ky(m)*yr).*exp(kxy*yr)).* ...
    %     (exp(kyx*xl).*cos(kx(n)*xl) + exp(kyx*xr).*cos(kx(n)*xr));
    A(2*nkx+1:2*nkx+2*nky,1:2*nkx)= A(2*nkx+1:2*nkx+2*nky,1:2*nkx) ...
        + beta* B';
    
    % beta * Phiy * conj(Phiy)
    [m,n]= ndgrid(1:nky,1:nky);
    kyxm= sqrt(ky(m).^2-k^2); kyxn= sqrt(ky(n).^2-k^2);
    % temp= [kyxm+kyxn, kyxm-kyxn; -kyxm+kyxn, -kyxm-kyxn];
    % By= cos(ky(m)*yl).*cos(ky(n)*yl) + cos(ky(m)*yr).*cos(ky(n)*yr); 
    % By=  [By By; By By]; 
    % A(2*nkx+1:2*nkx+2*nky,2*nkx+1:2*nkx+2*nky)= ...
    %     A(2*nkx+1:2*nkx+2*nky,2*nkx+1:2*nkx+2*nky)  ...
    %     -1.5i*k*alpha*By.*(exp(temp*xl)+exp(temp*xr));
    By= cos(ky(m)*yl).*cos(ky(n)*yl) + cos(ky(m)*yr).*cos(ky(n)*yr);
    Bxpp= exp((kyxm+kyxn)*(xl-xr)) + 1;
    Bxpm= exp(kyxm*(xl-xr)) + exp(kyxn*(xl-xr));
    A(2*nkx+1:2*nkx+2*nky,2*nkx+1:2*nkx+2*nky)= ...
        A(2*nkx+1:2*nkx+2*nky,2*nkx+1:2*nkx+2*nky) + ...
        beta* [Bxpp Bxpm; Bxpm Bxpp].*[By By; By By];
    
    % ---- end of second order terms at the corners
end

disp('det(A)'); disp(det(A)); disp('cond(A)'); disp(cond(A));
% -- end of assembly of the matrix of the linear algebra problem
%
% -- assemble the right hand side, which is the minus trace of the 
%    special solution tested by the plane wave basis
kx= kx(:); ky= ky(:);
Phi= @(x,y) [cos(kx*x).*exp(sqrt(kx.^2-k^2)*(y-yr)) % plane wave basis
             cos(kx*x).*exp(-sqrt(kx.^2-k^2)*(y-yl))
             cos(ky*y).*exp(sqrt(ky.^2-k^2)*(x-xr))
             cos(ky*y).*exp(-sqrt(ky.^2-k^2)*(x-xl))];
b= quadv(@(t) gxl(t).*conj(Phi(xl,t)) + gxr(t).*conj(Phi(xr,t)),yl,yr) + ...
   quadv(@(t) gyl(t).*conj(Phi(t,yl)) + gyr(t).*conj(Phi(t,yr)),xl,xr);
if alpha~=0  
   DxPhi= @(x,y) [-kx.*sin(kx*x).*exp(sqrt(kx.^2-k^2)*(y-yr))
                 -kx.*sin(kx*x).*exp(-sqrt(kx.^2-k^2)*(y-yl))
                 cos(ky*y).*exp(sqrt(ky.^2-k^2)*(x-xr)).*sqrt(ky.^2-k^2)
                 cos(ky*y).*exp(-sqrt(ky.^2-k^2)*(x-xl)).*(-sqrt(ky.^2-k^2))];
   DyPhi= @(x,y) [cos(kx*x).*exp(sqrt(kx.^2-k^2)*(y-yr)).*sqrt(kx.^2-k^2)
                 -cos(kx*x).*exp(-sqrt(kx.^2-k^2)*(y-yl)).*sqrt(kx.^2-k^2)
                 -ky.*sin(ky*y).*exp(sqrt(ky.^2-k^2)*(x-xr))
                 -ky.*sin(ky*y).*exp(-sqrt(ky.^2-k^2)*(x-xl))];
   b= b + quadv(@(t) g2xl(t).*conj(DyPhi(xl,t)) + ...
                g2xr(t).*conj(DyPhi(xr,t)),yl,yr) ...
     + quadv(@(t) g2yl(t).*conj(DxPhi(t,yl)) + ...
             g2yr(t).*conj(DxPhi(t,yr)),xl,xr);
   b= b + g3xlyl.*conj(Phi(xl,yl)) + g3xlyr.*conj(Phi(xl,yr)) +...
      g3xryl.*conj(Phi(xr,yl)) + g3xryr.*conj(Phi(xr,yr));
end

%   solve the coefficients of the plane wave basis
c= A\b;
figure(1), hold off, set(0,'defaultaxesfontsize',15);
semilogy(kx,abs(c(1:nkx)),'d-'); 
hold on, semilogy(kx,abs(c(nkx+1:2*nkx)),'g.-');
ylimit= ylim;
semilogy(repmat(k,11,1),linspace(ylimit(1),ylimit(2),11),'r-.');
xlabel('cos freq in x','fontsize',15), 
ylabel('modulus of the coefficients','fontsize',15);
legend('cos(k_xx)exp(sqrt(k_x^2-k^2)(y-yr))', ...
       'cos(k_xx)exp(-sqrt(k_x^2-k^2)(y-yl))',...
       'wavenumber', 'Location','best');
% h= legend; set(h,'interpreter','latex');
u= @(x,y) G(x-xs,y-ys) + c.'*Phi(x,y); % assume x, y are row vectors
if ~exist('x','var')
    x= linspace(xl,xr,101); % or, 20*floor((xr-xl)/2)+1); 
    y= linspace(yl,yr,101); % or, 20*floor((yr-yl)/2)+1);
end
[x,y]= meshgrid(x,y);
uh= u(x(:).',y(:).'); uh= reshape(uh,size(x,1),size(x,2));
figure(2), hold off, surfc(x,y,real(uh)); colormap cool, 
xlabel('x', 'fontsize',15), ylabel('y','fontsize',15);
end