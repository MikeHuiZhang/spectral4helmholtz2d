function A= setabc(A,SIZ,ix,iy,k,Dx,Dy,Dxx,Dyy)
%A= setabc(A,SIZ,ix,iy,iz,k)
% reset rows of A for the second order absorbing boundary condition
% A: In/Out 
%    the matrix
% SIZ: In 
%    the grids size consists of the numbers of nodes along x, y
% ix, iy: In
%    the 1d indices of an edge/vertice, for one call input only one 
%    edge/vertice, the 2d indices of which are tensor product of 
%    ix,iy 
% k: In
%    the wavenumber
% Dx, Dy, Dxx, Dyy: In
%    the differentiation matrices for 1d grids

% Nearly no tensor available because k may be not separable.

% inputs check
if isempty(ix) || isempty(iy)
    return;
end
sizeA= size(A);
if sizeA(1) ~= sizeA(2)
    disp('setabc(): the input matrix A is not square');
    return;
end
if size(SIZ)<2
    disp(['setabc(): input SIZ must contain two integers for number of',...
        'grid points along x, y']);
end
if sizeA(1) ~= prod(SIZ(1:2))
    disp('setabc(): product of the input SIZ(1:2) is not equal to nrow of A');
    return;
end
if max(ix)>SIZ(1) || min(ix)<1
    disp('setabc(): input ix exceeds the 1~SIZ(1)');
    return;
end
if max(iy)>SIZ(2) || min(iy)<1
    disp('setabc(): input iy exceeds the input 1~SIZ(2)');
    return;
end
if isnumeric(k) && ~isscalar(k)
    sizek= size(k);
    if prod(sizek(:))~=prod(SIZ(1:2))
        disp(['setabc(): input k is a (multi)vector but its length is', ...
            'not equal to product of SIZ(1:2)']);
        return;
    end
end
ix= unique(ix); iy= unique(iy); 
if length(ix)>1 && length(iy)>1 
    disp('setabc(): at least one of ix,iy must be a scalar');
end
isboundary= 0;
if length(ix)==1 
    if ix==1 || ix==SIZ(1)
         isboundary= 1;
    end
end
if length(iy)==1
    if iy==1 || iy==SIZ(2)
        isboundary= 1;
    end
end
if ~isboundary
    disp('setabc(): at least one of ix,iy,iz must be on the boundary');
end

% initialization
[IX,IY]= ndgrid(ix,iy);
rowA= IX(:) + (IY(:)-1)*SIZ(1);
A(rowA,:)= 0; 

% switch to the specific edge/vertice and set A
if length(ix)==1 && (1==ix || SIZ(1)==ix)
    signx= (ix==1)*(-1) + (ix==SIZ(1));
    if length(iy)==1 && (iy==1 || iy==SIZ(2)) % one of the vertices
        signy= (iy==1)*(-1) + (iy==SIZ(2)); 
        r0= 1.5;
        for irowA= 1:length(rowA)
            setDx();
            setDy();
            setD0();
        end
    else % an edge x==xl or x==xr
        r0 = 1;
        for irowA= 1:length(rowA)
            setDx();
            setD0();
            setDyy();
        end
    end
elseif length(iy)==1 && (iy==1 || iy==SIZ(2)) % an edge at y==yl, yr
    signy= (iy==1)*(-1) + (iy==SIZ(2));   
    r0= 1;
    for irowA= 1:length(rowA)
        setDy();
        setD0();
        setDxx();
    end    
end

    function setDx()
        colA= (1:SIZ(1))+(IY(irowA)-1)*SIZ(1);
        A(rowA(irowA),colA)= A(rowA(irowA),colA) + signx*Dx(ix,:);
    end

    function setDy()
        colA= IX(irowA) + (0:SIZ(2)-1)*SIZ(1);
        A(rowA(irowA),colA)= A(rowA(irowA),colA) + signy*Dy(iy,:);
    end

    function setD0()
        A(rowA(irowA),rowA(irowA))= A(rowA(irowA),rowA(irowA))-r0*1i*k;
    end

    function setDyy()
        colA= IX(irowA) + (0:SIZ(2)-1)*SIZ(1);
        A(rowA(irowA),colA)= A(rowA(irowA),colA) -0.5i/k*Dyy(IY(irowA),:);
    end

    function setDxx()
        colA= (1:SIZ(1)) + (IY(irowA)-1)*SIZ(1);
        A(rowA(irowA),colA)= A(rowA(irowA),colA) - 0.5i/k*Dxx(IX(irowA),:);
    end
end
