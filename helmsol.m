%helmsol.m
% Solve the Helmholtz equation 
%   u_{xx} + u_{yy} + k^2 u = f, in the rectangle [xl,xr]X[yl,yr]
% equipped with the second order boundary condition
%   u_n - 1i*k u -0.5i/k u_{ss} = 0,  in the edges,
%   u_{n1} + u_{n2} - 1.5i*k u  = 0,  at the corners,
% where 'n', 'n1' and 'n2' are outward normal directions, and 's' is
% tangential direction.
% Note that the sign for the ik or i/k comes from the time-harmonic
% ansatz u(t,x)= \sum_k exp(-ikt) u_k(x).

clear all;

% wavenumber
k= 4; k2= k^2;

% geometry
xl= -10; yl= -10; 
xr=  10; yr=  10; 

% source 
xs= xl + (xr-xl)/2;
ys= yl + (yr-yl)/2;
a= 1/2; 
r2= @(x,y) x.^2 + y.^2;
fs= @(x,y) -40*(r2(x,y)<=a^2).*exp(a^2./(r2(x,y)-a^2)); 

% wave based method
kx1= pi/(xr-xl); kkx = floor(k/kx1); % kkx + 1 is the number of cosine
ky1= pi/(yr-yl); kky = floor(k/ky1); % freq. less than wavenumber
kx= kx1*(0:4*kkx); ky= ky1*(0:4*kky);
[uhwav,x,y] = wavsol(xl,xr,yl,yr,k,xs,ys,kx,ky,2);
G= @(x,y) 0.25i*besselh(0,1,k.*sqrt(x.^2+y.^2)); % the Green function
erru2G= uhwav-G(x-xs,y-ys);
figure, surfc(x,y,real(erru2G)), xlabel('x','fontsize',15), 
        ylabel('y','fontsize',15);
figure, surfc(x,y,real(G(x,y)));


% cheb collocation method
% N= 20; % number of Chebyshev nodes in one dimension is N+1
% [A,x,y]= Assembly_Helmholtz(xl,xr,yl,yr,N,k2,k);
% [X,Y]= ndgrid(x,y);
% fh= zeros(size(X)); idx= 2:N;
% fh(idx,idx)= fs(X(idx,idx),Y(idx,idx)); 
% fh= fh(:);
% uhcheb= A\fh; 
% figure, surfc(Y,X,reshape(real(uhcheb),N+1,N+1));
