function A= setallabc(A,SIZ,k,Dx,Dy,Dxx,Dyy)
%A= setallabc(A,SIZ,k,Dx,Dy,Dxx,Dyy)
% set the matrix A for the grid of SIZ with the second-order a.b.c
% on all the boundary
ix= 2:SIZ(1)-1; iy= 2:SIZ(2)-1; 
A= setabc(A,SIZ,1,iy,k,Dx,Dy,Dxx,Dyy);      % edge x==xl
A= setabc(A,SIZ,SIZ(1),iy,k,Dx,Dy,Dxx,Dyy) ;% edge x==xr
A= setabc(A,SIZ,ix,1,k,Dx,Dy,Dxx,Dyy);      % edge y==yl
A= setabc(A,SIZ,ix,SIZ(2),k,Dx,Dy,Dxx,Dyy); % edge y==yr
% corners
A= setabc(A,SIZ,1,1,k,Dx,Dy,Dxx,Dyy);
A= setabc(A,SIZ,SIZ(1),1,k,Dx,Dy,Dxx,Dyy);
A= setabc(A,SIZ,1,SIZ(2),k,Dx,Dy,Dxx,Dyy);
A= setabc(A,SIZ,SIZ(1),SIZ(2),k,Dx,Dy,Dxx,Dyy);

end